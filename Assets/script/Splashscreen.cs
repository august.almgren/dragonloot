﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Splashscreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		string loserNum = PlayerPrefs.GetString("loserNum");
		GameObject.Find("Loser Text").GetComponent<Text>().text =
			"Player " + loserNum +
			" Stole The Most Coins And Was Eaten By The Dragon";
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("space"))
			SceneManager.LoadScene("Arena");
	}
}
