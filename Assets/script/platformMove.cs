﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformMove : MonoBehaviour 
{
	public float moveSpeed = 1;

	public Vector2 target;
	public Vector2 target2;

	private Vector2 currentPosi;
	private Vector2 target3; 

	void Start()
	{
		target3 = target;
	}
	void Update () 
	{
		currentPosi = transform.position;
		if (currentPosi != target)
		{
			transform.position = Vector2.MoveTowards (currentPosi, target, (moveSpeed * Time.deltaTime));
		} 
		else
		{
			target = target2;
			if (currentPosi == target) 
			{
				target = target3;
			}
		}

	}
}
