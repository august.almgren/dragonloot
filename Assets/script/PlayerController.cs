﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	public static int loserNum = 0;
	public static int loserCoins = 0;

	public int playerNum = 0;
	public int numCoins = 0;
	public float moveSpeed;
	public float jumpForce;
	public KeyCode left;
	public KeyCode right;
	public KeyCode jump;
	public KeyCode throwCoin;
	public float gravity = 20.0f;
	bool facingRight = false;
	public string coinsTextID;

	public Transform groundCheckPoint;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public bool isGrounded;

	public bool throwCoins = true;
	public int numCoinsThrown = 1;
	public float coinThrowCooldown = 0;
	public Vector2 throwForce;

	private Rigidbody2D rigid2D;
	private float nextCoinThrow;
	Animator anim;

	private float radius;
	private GameObject coins;

	// Use this for initialization
	void Start () {
		rigid2D = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator> ();
		throwCoins = true;
		nextCoinThrow = 0;
		radius = GetComponent<Renderer>().bounds.extents.magnitude;
		coins = transform.Find("Coins").gameObject;
	}


	void Update()
	{
		// teleport player to other side when leaving left/right camera edges
		float camxedge = Camera.main.orthographicSize * 2;
		float xpos = gameObject.transform.position.x;
		float leftEdge = xpos - radius;
		float rightEdge = xpos + radius;

		if (rightEdge < -camxedge) {
			transform.position = new Vector2(camxedge, transform.position.y);
		} else if (leftEdge > camxedge) {
			transform.position = new Vector2(-camxedge, transform.position.y);
		}

		isGrounded = Physics2D.OverlapCircle (groundCheckPoint.position, groundCheckRadius, whatIsGround);
		if (Input.GetKey (left) || Input.GetKey (right)) {
			anim.SetBool ("Speed", true);
		} else {
			anim.SetBool ("Speed", false);
		}
		if (Input.GetKey (left)) {
			rigid2D.velocity = new Vector2 (-moveSpeed, rigid2D.velocity.y);
			if (facingRight) {
				transform.localRotation = Quaternion.Euler(0, 180, 0);
				coins.transform.localRotation = Quaternion.Euler(180, 0, 180);
				Vector3 pos = coins.transform.position;
				pos.z = -1;
				coins.transform.position = pos;
			}
			facingRight = false;
		} else if (Input.GetKey (right)) {
			rigid2D.velocity = new Vector2 (moveSpeed, rigid2D.velocity.y);
			if (!facingRight) {
				transform.localRotation = Quaternion.Euler(0, 0, 0);
				coins.transform.localRotation = Quaternion.Euler(0, 0, 0);
				Vector3 pos = coins.transform.position;
				pos.z = -1;
				coins.transform.position = pos;
			}
			facingRight = true;
		}
		else {
			rigid2D.velocity = new Vector2 (0, rigid2D.velocity.y);
		}

		if (Input.GetKey (jump)&&isGrounded) {
			rigid2D.velocity = new Vector2 (rigid2D.velocity.x,jumpForce);
		}
		else if (Input.GetKeyDown(throwCoin) && throwCoins && Game.playTime >= nextCoinThrow)
		{
			nextCoinThrow = Game.playTime + coinThrowCooldown;

			throwCoins = false;
			for (int i = 0; i < numCoinsThrown; i++) {
				ThrowCoin();
			}
		}
		else if (Input.GetKeyUp(throwCoin))
		{
			throwCoins = true;
		}
	}

	void ThrowCoin()
	{
		if (numCoins == 0)
			return;

		Vector2 pos = rigid2D.position;
		if (facingRight)
			pos.x += 1.2f;
		else
			pos.x -= 1.2f;
		pos.y += 0;

		GameObject coin = Instantiate(
			Coin.prototype,
			pos,
			Quaternion.identity
		) as GameObject;

		//Vector2 force = new Vector2(facingRight ? 16f : -16f, 4f);
		Vector2 force = new Vector2(
				facingRight ? throwForce.x : -throwForce.x, throwForce.y);
		coin.GetComponent<Rigidbody2D>().velocity = force;

		updateNumCoins(-1);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Coin") {
			Destroy(other.gameObject);
			updateNumCoins(1);
		} else if (other.gameObject.tag == "Powerup") {
			Powerup powerup = other.gameObject.GetComponent<Powerup>();
			//if (numCoins >= powerup.cost) {
				//updateNumCoins(-powerup.cost);
				updateNumCoins(powerup.cost);
				addPowerup(powerup);
				Destroy(other.gameObject);
			//}

		}
	}

	private void addPowerup(Powerup powerup) {
		switch (powerup.type) {
			case Powerup.Type.COINS:
				numCoinsThrown += (int)powerup.modifier;
				break;
			case Powerup.Type.THROW:
				throwForce.x += powerup.modifier;
				break;
			case Powerup.Type.JUMP:
				jumpForce += powerup.modifier;
				break;
			case Powerup.Type.SPEED:
				moveSpeed += powerup.modifier;
				break;
			case Powerup.Type.COOLDOWN:
				coinThrowCooldown -= powerup.modifier;
				break;
				/*
			case Powerup.Type.RANDOM:
				addPowerup((Powerup.Type)Random.Range(0, (int)(type - 1)), );
				break;
				*/
		}

	}
	public void updateNumCoins(int delta)
	{
		numCoins += delta;
		// TODO create a separate text for each player
		//GameObject.Find(coinsTextID).GetComponent<Text>().text = numCoins.ToString();
		transform.Find("Coins").GetComponent<Text>().text = numCoins.ToString();

		if (Game.tiebreak && delta > 0)
		{
			Game.tiebreak = false;
			loserNum = playerNum;
			loserCoins = numCoins;
		}
		else if (numCoins > loserCoins)
		{
			loserNum = playerNum;
			loserCoins = numCoins;
		}
		else if (numCoins == loserCoins)
		{
			loserNum = 0;
		}
	}
}
