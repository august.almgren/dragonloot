﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour {

	private float lastCoinSpawnTime;
	private float lastPowerupSpawnTime;

	public Vector2[] coinSpawners;
	public float coinSpawnRate;
	public Vector2[] powerupSpawners;
	public float powerupSpawnRate;
	public GameObject[] powerups;
	public int roundLength;
	public static float playTime;
	public int minCoinsPerSpawn;
	public int maxCoinsPerSpawn;
	public static bool tiebreak;
	
	// Use this for initialization
	void Start () {
		Coin.prototype = Resources.Load("Coin") as GameObject;
		PlayerController.loserNum = 0;
		PlayerController.loserCoins = 0;
		tiebreak = false;
		lastCoinSpawnTime = 0;
		lastPowerupSpawnTime = 0;
		playTime = 0;
	}
	
	// Update is called once per frame
	void Update () {

		// update time
		playTime += Time.deltaTime;
		int timeLeft = (int)(roundLength - playTime + 1);

		// check+handle gameover/tiebreak
		if (timeLeft <= 0)
		{
			// tiebreak
			if (PlayerController.loserNum == 0)
			{
				if (!tiebreak)
				{
					tiebreak = true;
					GameObject.Find("Timer Num").GetComponent<Text>().text =
						"00:00";
				}
				return;
			}

			PlayerPrefs.SetString("loserNum", PlayerController.loserNum.ToString());
			SceneManager.LoadScene("Results");
		}

		// update UI timer
		string minutes = ((timeLeft) / 60).ToString();
		if (minutes.Length == 1)
			minutes = "0" + minutes;

		string seconds = ((timeLeft) % 60).ToString();
		if (seconds.Length == 1)
			seconds = "0" + seconds;

		GameObject.Find("Timer Num").GetComponent<Text>().text = minutes + ":" + seconds;

		// spawn coins
		if (playTime >= lastCoinSpawnTime + coinSpawnRate) {
			lastCoinSpawnTime = playTime;
			int coinsToSpawn = Random.Range(minCoinsPerSpawn - 1, maxCoinsPerSpawn);
			for (int i = coinsToSpawn; i < maxCoinsPerSpawn; i++) {
				Instantiate(
					Coin.prototype,
					coinSpawners[Random.Range(0, coinSpawners.Length)],
					Quaternion.identity
				);
			}
		}

		// spawn powerups
		if (playTime >= lastPowerupSpawnTime + powerupSpawnRate) {
			lastPowerupSpawnTime = playTime;
			GameObject powerup = powerups[Random.Range(0, powerups.Length)];
			Instantiate(
				powerup,
				powerupSpawners[Random.Range(0, powerupSpawners.Length)],
				Quaternion.identity
			);
		}
	}
}
