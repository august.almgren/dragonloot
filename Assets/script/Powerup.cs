﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Powerup : MonoBehaviour {

	public static GameObject prototype;

	public enum Type { COINS, THROW, SPEED, JUMP, COOLDOWN, RANDOM };
	public Type type;
	public int cost;
	public float modifier;

	private float radius;

	// Use this for initialization
	void Start () {
		// TODO load resource statically and attach to prototype
		GameObject price = (GameObject)Instantiate(Resources.Load("Price"));
		Text priceText = price.GetComponent<Text>();
		priceText.text = cost.ToString();
		price.transform.SetParent(gameObject.transform);
		Vector3 pos = gameObject.transform.position;
		pos.y += 0.05f;
		pos.z = -1f;
		price.transform.position = pos;
		radius = GetComponent<Renderer>().bounds.extents.magnitude;
	}

	// Update is called once per frame
	void Update () {
		float camxedge = Camera.main.orthographicSize * 2;
		float xpos = gameObject.transform.position.x;
		float leftEdge = xpos - radius;
		float rightEdge = xpos + radius;

		if (rightEdge < -camxedge) {
			transform.position = new Vector2(camxedge, transform.position.y);
		} else if (leftEdge > camxedge) {
			transform.position = new Vector2(-camxedge, transform.position.y);
		}
	}
}
