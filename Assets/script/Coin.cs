﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	public static GameObject prototype;
	private float radius;

	// Use this for initialization
	void Start () {
		radius = GetComponent<Renderer>().bounds.extents.magnitude;
	}
	
	// Update is called once per frame
	void Update () {
		float camxedge = Camera.main.orthographicSize * 2;
		float xpos = gameObject.transform.position.x;
		float leftEdge = xpos - radius;
		float rightEdge = xpos + radius;

		if (rightEdge < -camxedge) {
			transform.position = new Vector2(camxedge, transform.position.y);
		} else if (leftEdge > camxedge) {
			transform.position = new Vector2(-camxedge, transform.position.y);
		}
	}
}
