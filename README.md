# About

Dragonloot is a 4 player game made using Unity. Each round of the game lasts for 2 minutes, and the player with the most coins at the end of a round is eaten by the dragon, losing the game.

Players are able to move left and right, jump, and throw coins which they have collected. A number of powerups also spawn intermittently. Pickup up these will increase the player's coins by 15, while at the same time increasing the player's jump height, run speed, coin throw power, number of coins thrown at a time, or decrease the cooldown time between coin throws.

This project was made in a group as a prototype over the course of a week. The goal was to create a game which featured some sort of resource management, which was accomplished by introducing a tradeoff between rewarding players for picking up coins (by gaining powerups and being able to throw them at other players) and at the same time discouraging the same behavior (as the player with the most coins at the end of the game loses).

# Controls

| Player   | Jump   | Left    | Throw  | Right   |
|----------|--------|---------|--------|---------|
| `1`      | `W`    | `A`     | `S`    | `D`     |
| `2`      | `I`    | `J`     | `K`    | `L`     |
| `3`      | `T`    | `F`     | `G`    | `H`     |
| `4`      | `UP`   | `LFFT`  | `DOWN` | `RIGHT` |

# Sources

* [Background Image](https://search.creativecommons.org/photos/3e57943d-07c5-40e6-8b4d-df146366229c)
* [Dinosaur Sprites](https://arks.itch.io/dino-characters)